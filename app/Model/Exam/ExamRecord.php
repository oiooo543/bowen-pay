<?php

namespace App\Model\Exam;

use Illuminate\Database\Eloquent\Model;

class ExamRecord extends Model
{
    protected $fillable = ['name', 'matric', 'status', 'profileurl'];
}
