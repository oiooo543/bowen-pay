<?php

namespace App\Model\Attendance;

use Illuminate\Database\Eloquent\Model;

class HostelList extends Model
{
    protected $fillable = ['name'];
}
