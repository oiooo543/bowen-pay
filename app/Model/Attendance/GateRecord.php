<?php

namespace App\Model\Attendance;

use Illuminate\Database\Eloquent\Model;

class GateRecord extends Model
{
    protected $fillable = ['time', 'sid', 'direction'];
}
