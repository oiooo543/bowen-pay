<?php

namespace App\Model\Attendance;

use Illuminate\Database\Eloquent\Model;

class HostelAttendance extends Model
{
    protected $fillable = ["home", "mat_no", "time", "staff"];
}
