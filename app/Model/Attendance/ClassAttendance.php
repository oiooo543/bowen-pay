<?php

namespace App\Model\Attendance;

use Illuminate\Database\Eloquent\Model;

class ClassAttendance extends Model
{
    protected $fillable = ['course', 'time', 'matricno'];
}
