<?php

namespace App\Model\Payment;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['txIp', 'txRef', 'raveRef', 'amount', 'appFee', 'paymenttype', 'orderRef', 'status', 'nregion', 'outside', 'apptype', 'dest', 'orderStatus'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
