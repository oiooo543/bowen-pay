<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class AuthController extends Controller
{
    public function store(Request $request){



        $name = $request->input('name');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $mat_no = $request->input('mat_no');
        $password = $request->input('password');
        $dept = $request->input('dept');
        $prog = $request->input('prog');
        $faculty = $request->input('faculty');


        $user = new User([
            'name' => $name,
            'phone' => $phone,
            'mat_no' => $mat_no,
            'email' => $email,
            'dept' => $dept,
            'prog' => $prog,
            'faculty' => $faculty,
            'password' => bcrypt($password)
        ]);

      //  return $user;
        //$bio = User::find(1)->biodata;



        try{

            if ( $user->save()){
                $response = "User created successfully";
                return  response()->json($response, 201);
            }else {

                $response = [
                    'msg' => "Registration Failed"

                ];
                return response()->json($response, 404);

            }

        }catch (QueryException $e){
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062){
                return response()->json("You are already registered", "400");
            }
        }



    }

    public function signin(Request $request){



        $email = $request->input('email');

        $credentials = $request->only('email', 'password');

       try{
           if (! $token = JWTAuth::attempt($credentials)){
               return response()->json("User Not Found", "401");
           }
       } catch (JWTException $e) {
           return response()->json(['status' => 400], 500);
       }

        $users = User::all()->where('email', $email);

       foreach ($users as $user){

        if($user['status'] == 'suspended') {

            return response()->json('You are suspended see admin', 402);
        }
       }

       $name = '';
       $phone = '';
       $category = '';
       $mat_no = '';
       $email = '';
       $id = '';
       $type = '';

        foreach ($users as $user) {
            $id = $user->id;
             $name =   $user->name;
             $phone =   $user->phone;
             $category =   $user->type;
             $mat_no =  $user->mat_no;
             $email = $user->email;
             $type = $user->type;


        }

        //return $users;
        $used = [ 'type'=> $type, 'name' => $name, 'phone' => $phone, 'category' => $category, 'mat_no' => $mat_no, 'id' => $id, 'email' => $email];

        return response()->json(['token' => $token, 'user' => $used] );

    }

    public  function getUser() {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('You are not authorized', 401);
        }

        if ($user->type != 'admin') {

            return response('Not Authorized', 404);
        }

        $users = User::all()->sortBy('created_at', SORT_DESC);

        if ($users != null) {

            return response()->json($users, 200);
        }else {
            return response()->json('No user found', 404);
        }
    }

    public function  editUser(Request $request, $id) {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('You are not authorized', 401);
        }

        $users = User::find($id);

       // $users = $request->all();

        $users->name =  $request->input('name');
        $users->email = $request->input('email');
        $users->password = bcrypt($request->input('password'));
        $users->mat_no = $request->input('mat_no');
        $users->phone = $request->input('phone');
        $users->type = $request->input('type');

        if ($users != null){
            if ($users->update()){
                return response()->json('update successful', 202);
            }else{
                return response()->json('update failed', 401);
            }
        }else{
            return response()->json('bio not found', 404);
        }
    }

    public function  suspendUser(Request $request, $id) {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('You are not authorized', 401);
        }

        $users = User::find($id);

        // $users = $request->all();

        $users->name =  $request->input('name');
        $users->email = $request->input('email');
        $users->password = bcrypt($request->input('password'));
        $users->mat_no = $request->input('mat_no');
        $users->phone = $request->input('phone');
        $users->type = $request->input('type');
        $users->status = $request->input('status');

        if ($users != null){
            if ($users->update()){
                return response()->json('update successful', 202);
            }else{
                return response()->json('update failed', 401);
            }
        }else{
            return response()->json('bio not found', 404);
        }
    }

    public function getSecUser(){

        return array('res' => $users = User::where('dept', 'sec')->get(['id', 'email', 'mat_no']));
    }

}
