<?php

namespace App\Http\Controllers\Exam;

use App\Exam\Exam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Exam::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $exam = new Exam($request->all());

        $exams = $request->all();

        if ($exams == null){
            return response()->json('Empty list', 400);
        }

        //return $exams;
        $con = 0;
        $conn = 0;

        foreach ($exams as $exam){

            $exam = new Exam($exam);
            if ($exam->save()) {
                $con++;
            }
            $conn ++;
        }

        $result = ['stat' => 'successful'];

        if ($con >= 1){
            return response()->json(array( ['Saved' => $con], ['num'=> $con]), 200);
        }



        $result = ['stat' => 'failed'];

        return response()->json(array('res' => $result), 400);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
