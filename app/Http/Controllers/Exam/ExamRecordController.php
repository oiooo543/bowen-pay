<?php

namespace App\Http\Controllers\Exam;

use App\Model\Exam\ExamRecord;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $records = ExamRecord::all();

       $finalRecord = null;
       foreach ($records as $record) {

           $finalRecord [] = $record;
       }

       $student = array('students' => $finalRecord);

       if ($records->isNotEmpty()) {
           return response()->json($student, 200);
       } else {
           return response()->json('data not found', 400);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $examrecord = new ExamRecord($request->all());



        if($examrecord->save()) {
            return response()->json('The data inserted successfully', 200);
        } else {
            return response()->json('data insertion failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
