<?php

namespace App\Http\Controllers\Gate;

use App\Model\Attendance\GateRecord;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GateSpecController extends Controller
{
    public function deleteAll() {
        if (GateRecord::truncate()) {
            return response()->json('', 200);
        }
    }

    public function totalIn(){
        $in = GateRecord::where('direction', 'IN')->count();
        $out = GateRecord::where('direction', 'OUT')->count();
        $total = GateRecord::all()->count();

        $tip = [
            'in' => $in,
            'out' => $out,
            'total' => $in + $out
        ];

        return response()->json($tip, 200);
    }

    public function categorySum() {

        $ven [] = null;
        $sch [] = null;
        $caf [] = null;

        $sids = GateRecord::all();

        //return $sids;

        foreach ($sids as $sid) {

            $key = explode('/', $sid['sid']);

          //  return $key;

            if ($key[1] == 'VEN') {

                array_push($ven, $sid['sid']);
            }

            if ($key[1] == 'SCH') {

                array_push($sch, $sid['sid']);
            }

            if ($key[1] == 'CAF') {

                array_push($caf, $sid['sid']);
            }
        }

        $pick = [$ven, $caf, $sch];

        return response()->json($pick, 200);
    }

    public function getDayReport($date) {

        $datereport = GateRecord::whereDate('created_at', $date)->get();

        return response()->json();
    }

}
