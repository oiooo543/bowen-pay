<?php

namespace App\Http\Controllers\Payment;

use App\Model\Payment\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class PaymentsSpecController extends Controller
{
    public function getIndividual() {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $payments = Payment::all()->where('user_id', $user)->sortByDesc('created_at', 3)->get();

        if ($payments != null ){
            return response()->json('No payment found', 200);
        }
        $details = null;
        foreach ($payments as $payment ) {
            $detail = [ 'amount' => $payment['amount'], 'txRef' => $payment['$payment'],
                        'paymenttype'=> $payment['paymenttype'], 'status'=> $payment['status'],
                        'dest'=> $payment['dest'], 'apptype' => $payment['apptype'],
                        'id' => $payment['id'], 'date'=> $payment['created_at']];

            $details[] = $detail;
        }

       return response()->json($details, 200);
    }

    public function todayApplication() {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $now = Carbon::now();

        $applications = Payment::with('user')->where('orderStatus', 'Processing')->whereDate('created_at', $now->toDateString())->get();

      //  return $now->toDateString();

        if ($applications != null) {
            return response()->json($applications, 200);
        } else {
            return response()->json('No data found', 404);
        }
    }

    public function getTodayPay() {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

         $payments = Payment::all();
        $amounts = null;
        $total = null;

            foreach ($payments as $payment) {
                $amounts = $payment['amount'];
              //  $total = $amounts;

                $total = $total + $amounts;
            }

            if ($total != null) {
                return response()->json($total, 200);
            } else {
                return response()->json('no money found', 400);
            }

    }

    public function todayPayApplication() {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $now = Carbon::now();

        $applications = Payment::with('user')->where('orderStatus', 'Processing')->whereDate('created_at', $now->toDateString())->get();

        $total = null;
        foreach ($applications as $payment) {
            $amounts = $payment['amount'];
            //  $total = $amounts;

            $total = $total + $amounts;
        }
        if ($applications != null) {
            return response()->json($applications, 200);
        } else {
            return response()->json('No data found', 404);
        }
    }

    public function monthAnalysis() {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

       $payments = Payment::where('status', 'successful')->get();

        $attestotal = null; $academictotal= null; $addtotal = null; $certiTotal = null; $profcertification = null;
        $letterTotal = null; $absorbtotal = null; $veritotal = null; $vericertiTotal = null; $trans = null;

        foreach ($payments as $payment) {

            if ($payment['apptype'] == 'Attestation Letter Fee') {
                $attestotal =  $attestotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Academic Record Fee') {
                $academictotal = $academictotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Add and Delete Fee') {
                $addtotal = $addtotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Certification') {
                $certiTotal = $certiTotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Proficiency Letter') {
                $profcertification = $profcertification + $payment['amount'];
            }
            if ($payment['apptype'] == 'Letter of Reference') {
                $letterTotal = $letterTotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Reabsorbtion Fee') {
                $absorbtotal = $absorbtotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Transcript Application') {
                $trans = $trans + $payment['amount'];
            }
            if ($payment['apptype'] == 'Verification of transcript') {
                $veritotal = $veritotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Verification of Certificate') {
                $vericertiTotal = $vericertiTotal + $payment['amount'];
            }
        }

        $details = array('certiTotal' => $vericertiTotal, 'trans' => $trans, 'veritotal' => $veritotal, 'absorb' => $absorbtotal, 'letter' => $letterTotal,
            'profcerti' => $profcertification, 'certificate' => $certiTotal, 'addTotal' => $addtotal, 'academic' => $academictotal,
            'attest' => $attestotal
            );

        return response()->json($details, 200);
    }

    public function weekAnalysis() {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $now = Carbon::now();

        $payments = Payment::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();;

      //  return $payments;

        $attestotal = null; $academictotal= null; $addtotal = null; $certiTotal = null; $profcertification = null;
        $letterTotal = null; $absorbtotal = null; $veritotal = null; $vericertiTotal = null; $trans = null;

        foreach ($payments as $payment) {

            if ($payment['apptype'] == 'Attestation Letter Fee') {
                $attestotal =  $attestotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Academic Record Fee') {
                $academictotal = $academictotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Add and Delete Fee') {
                $addtotal = $addtotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Certification') {
                $certiTotal = $certiTotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Proficiency Letter') {
                $profcertification = $profcertification + $payment['amount'];
            }
            if ($payment['apptype'] == 'Letter of Reference') {
                $letterTotal = $letterTotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Reabsorbtion Fee') {
                $absorbtotal = $absorbtotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Transcript Application') {
                $trans = $trans + $payment['amount'];
            }
            if ($payment['apptype'] == 'Verification of transcript') {
                $veritotal = $veritotal + $payment['amount'];
            }
            if ($payment['apptype'] == 'Verification of Certificate') {
                $vericertiTotal = $vericertiTotal + $payment['amount'];
            }
        }

        $details = array('certiTotal' => $vericertiTotal, 'trans' => $trans, 'veritotal' => $veritotal, 'absorb' => $absorbtotal, 'letter' => $letterTotal,
            'profcerti' => $profcertification, 'certificate' => $certiTotal, 'addTotal' => $addtotal, 'academic' => $academictotal,
            'attest' => $attestotal
        );

        return response()->json($details, 200);
    }

    public function courierWeek() {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $payments = Payment::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();

       // return $payments;

        $sw = null; $others = null; $wa = null; $rof = null; $uc = null; $uk = null;
        $sa = null; $me = null; $aa = null; $me = null; $la = null;

        foreach ($payments as $payment) {

            if ($payment['nregion'] == 'South-West'){
                    $sw++;
            }

            if ($payment['nregion'] == 'Others'){
                $others++;
            }

            if ($payment['outside'] == 'WEST AFRICA'){
                $wa++;
            }
            if ($payment['outside'] == 'REST OF AFRICA'){
                $rof++;
            }
            if ($payment['outside'] == 'USA/CANADA'){
                $uc++;
            }
            if ($payment['outside'] == 'UNITED KINGDOM/EU'){
                $uk;
            }
            if ($payment['outside'] == 'SOUTH AFRICA'){
                $sa++;
            }
            if ($payment['outside'] == 'MIDDLE EAST'){
                $me++;
            }
            if ($payment['outside'] == 'ASIA & AUSTRALIA'){
                $aa++;
            }
            if ($payment['outside'] == 'LATIN AMERICA & REST PART OF THE WORLD'){
                $la++;
            }
        }

        $details = array('la' => $la, 'aa' => $aa, 'me' => $me, 'sa' => $sa, 'uk'=> $uk, 'uc' => $uc, 'rof' => $rof, 'wa' => $wa,
                        'others' => $others, 'sw' => $sw);

        return response()->json($details, 200);

    }
}
