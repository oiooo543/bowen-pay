<?php

namespace App\Http\Controllers\Payment;

use App\Model\Payment\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $detail = null;
        $details = null;
        $payments = Payment::with('user')->get();

            foreach ($payments as $payment) {

                    $binta = Carbon::createFromFormat('Y-m-d H:i:s', $payment['created_at'])->format('Y-m-d');
                $detail = array('id' => $payment['id'], 'txRef' => $payment['txRef'], 'amount' => $payment['amount'], 'paymenttype' => $payment['paymenttype'],

                        'orderStatus' => $payment['orderStatus'], 'nregion' => $payment['nregion'], 'outside' => $payment['outside'], 'status' => $payment['status'], 'apptype' => $payment['apptype'], 'dest' => $payment['dest'],
                        'name' => $payment['user']['name'], 'mat_no' => $payment['user']['mat_no'], 'date' => $binta);

                $details [] = $detail;
            }

        if ($payments != null) {
            return response($details, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $payDetails = new Payment($request->all());

        if ($user->payment()->save($payDetails)) {
            return response()->json('Form submission successful', 201);
        } else {
            return response()->json('form submission failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $payments = Payment::find($id);

        if ($payments == null) {
            return response()->json('User not found', 400);
        }
        $payments->orderStatus = $request->input('appstatus');

        if ( $payments->update() ){
            return response()->json('Update successful', 200);
        } else {
            return response()->json('Update failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
