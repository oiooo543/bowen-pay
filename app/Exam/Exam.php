<?php

namespace App\Exam;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = [ 'matric', 'venue', 'course', 'time'];
}
