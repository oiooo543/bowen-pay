<?php

namespace App;

use App\Model\Attendance\GateRecord;
use Illuminate\Database\Eloquent\Model;

class ComReg extends Model
{
   protected $fillable = [ 'title', 'card_id', 'name', 'bname', 'slocation', 'marital', 'phone', 'email', 'sex', 'haddress', 'nokname', 'nokphonenumber'
   , 'nokhomeaddress', 'guaname', 'guaphonenumber', 'guahomeaddress', 'gua1name', 'gua1phonenumber', 'gua1homeaddress'];

   public function gateRecord() {
       return $this->hasMany(GateRecord::class, 'card_id');
   }
}
