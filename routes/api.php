<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('alpha')->group(function () {

    Route::get('admin/cat', [
        'uses' => 'Gate\GateSpecController@categorySum'
    ]);

    Route::get('admin/daily/sum', [
        'uses' => 'Gate\GateSpecController@totalIn'
    ]);

    Route::delete('clsgaterec', [
        'uses' => 'Gate\GateSpecController@deleteAll'
    ]);

    Route::resource('camreg', 'ComRegController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('hostelattend', 'Attendance\Hostel\HostelAttendanceController', [
        'except' => ['edit', 'create']
    ]);


    Route::resource('classattend', 'Attendance\Clas\ClassAttendanceController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('hostellist', 'Attendance\Hostel\HostelListController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('examattend', 'Exam\ExamController', [
        'except' => ['edit', 'create']
    ]);

    Route::get('secusers', [
        'uses' => 'AuthController@getSecUser'
    ]);

    Route::resource('attendance/gate', 'Attendance\GateRecordController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('exam/record', 'Exam\ExamRecordController', [
        'except' => ['edit', 'create']
    ]);

    Route::post('user', [
        'uses' => 'AuthController@store'
    ]);

    Route::post('user/signin', [
        'uses' => 'AuthController@signin'
    ]);

    Route::patch('user/edit/{id}', [
            'uses' => 'AuthController@editUser'
        ]);

    Route::get('reg/today', [
        'uses' => 'Payment\PaymentsSpecController@todayApplication'
    ]);

    Route::resource('user/payment', 'Payment\PaymentController', [
        'except' => ['edit', 'create']
    ]);

    Route::get('bus/total', [
        'uses' => 'Payment\PaymentsSpecController@getTodayPay'
    ]);

    Route::get('bus/today', [
        'uses' => 'Payment\PaymentsSpecController@todayPayApplication'
    ]);

    Route::get('bus/summary', [
        'uses' => 'Payment\PaymentsSpecController@monthAnalysis'
    ]);

    Route::get('bus/week', [
        'uses' => 'Payment\PaymentsSpecController@weekAnalysis'
    ]);

    Route::get('bus/weekMoney', [
        'uses' => 'Payment\PaymentsSpecController@courierWeek'
    ]);




});

    Route::group([
        'namespace' => 'Auth',
        'middleware' => 'api',
        'prefix' => 'password'
    ], function () {
        Route::post('create', 'PasswordResetController@create');
        Route::get('find/{token}', 'PasswordResetController@find');
        Route::post('reset', 'PasswordResetController@reset');
    });


