<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComRegsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('com_regs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('name');
            $table->string('bname');
            $table->string('slocation');
            $table->string('marital');
            $table->string('phone');
            $table->string('email');
            $table->string('sex');
            $table->string('card_id');
            $table->string('haddress');
            $table->string('nokname');
            $table->string('nokphonenumber');
            $table->string('gua1homeaddress');
            $table->string('gua1phonenumber');
            $table->string('gua1name');
            $table->string('guahomeaddress');
            $table->string('guaphonenumber');
            $table->string('guaname');
            $table->string('nokhomeaddress');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('com_regs');
    }
}
