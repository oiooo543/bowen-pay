<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('txIp');
            $table->string('txRef');
            $table->string('raveRef');
            $table->string('amount');
            $table->string('appFee');
            $table->string('paymenttype');
            $table->string('nregion')->nullable();
            $table->string('outside')->nullable();
            $table->string('orderRef');
            $table->string('status');
            $table->string('apptype');
            $table->string('dest')->default('No Sending');
            $table->string('orderStatus')->default('Processing');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
